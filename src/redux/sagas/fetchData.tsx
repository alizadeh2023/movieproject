import axios from 'axios';

const NFetch =  (url: string) => {
  // console.log(url, method, params, body)
  const Url = url;
  const config = {    
    headers: { 
      "Content-Type": "application/x-www-form-urlencoded; charset=utf-8",
      "Accept": "application/json"
    }
  };
  let data = axios.request<void, string>({
    method: 'get',
    url: Url,
  });
    return data

};
export default  NFetch ;
