import { put, takeEvery, takeLatest } from "redux-saga/effects";
import * as ActionsTypes from "../actions/actionTypes";
import NFetch from "./fetchData";

export interface ResponseGenerator{
    config?:any,
    data?:any,
    headers?:any,
    request?:any,
    status?:number,
    statusText?:string
}

const movieUseQuery = async (input:any) => {
    console.log(input)
    let title;
    var a = "";
    for (const [key, value] of Object.entries(input)) {

    if (`${value}` !== "")
    {
        title = key.charAt(0);
        console.log(title)
        a = a + `&`+ title + `=${value}`; 
    }
    
    }
    const url = `https://www.omdbapi.com/?apikey=39c36892` + a ;
    const result = await NFetch( url , 'GET' )
    return result ;
}
 
function* movieRequestHandler(action: { payload: any; }) { 
    try {
        const result:ResponseGenerator = yield movieUseQuery(action.payload);
        yield put({ type: ActionsTypes.MovieSUCCEEDED, Data: result.data ,  message:result.Error });

    } catch (e) {
        yield put({ type: ActionsTypes.MovieFAILED, message: e.message });
    }

}

export function* MovieSaga() {
    yield takeEvery(ActionsTypes.MovieREQUESTED, movieRequestHandler);
}


