import * as ActionsTypes from "./actionTypes";

export const MovieRequested = (input: any) => ({payload: input , type: ActionsTypes.MovieREQUESTED })
export const MovieSucceeded = () => ({ type: ActionsTypes.MovieSUCCEEDED })
export const MovieFailed = () => ({ type: ActionsTypes.MovieFAILED })