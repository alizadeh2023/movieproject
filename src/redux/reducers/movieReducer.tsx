import * as ActionsTypes from "../actions/actionTypes";

const initialState = {
    data: null,
    loading: false,
    message: ""
};

function MovieReducer(state = initialState, action: { type: any; Data: any; message: any; }) {
    switch (action.type) {
        /* Get User Profile APi Actions */
        case ActionsTypes.MovieREQUESTED:
            return { ...state, loading: true };
        case ActionsTypes.MovieSUCCEEDED:
            return { ...state, data: action.Data, loading: false, message: "" };
        case ActionsTypes.MovieFAILED:
            return { ...state, data: null, loading: false, message: action.message };

        default:
            return state;
    }
}

export default MovieReducer; 
