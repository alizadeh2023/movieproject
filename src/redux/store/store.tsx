import { createStore, applyMiddleware } from "redux";
import { persistStore, persistReducer } from "redux-persist";

import AsyncStorage from '@react-native-async-storage/async-storage';
import createSagaMiddleware from "redux-saga";

import rootReducer from "../reducers";
import rootSaga from "../sagas";


const persistConfig = {
    key: "root",
    storage: AsyncStorage,
    whitelist: ['movie']
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
const sagaMiddleware = createSagaMiddleware();


export let store = createStore(persistedReducer, applyMiddleware(sagaMiddleware));
export let persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

