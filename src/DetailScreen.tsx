
import React, { useState } from 'react';
import { Text, Image, TouchableOpacity } from "react-native";


const DetailsScreen = ({navigation}: {navigation: any}) => {

    return (
      <TouchableOpacity onPress={()=>navigation.navigate('Home')} style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
        <Text>Details Screen</Text>
      </TouchableOpacity>
    );
  }
  export default DetailsScreen