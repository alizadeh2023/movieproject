
import React, { useEffect , useState ,FC ,useMemo  } from 'react';
import { Text, Image, TouchableOpacity , View , FlatList, TextInput , Alert, ScrollView} from "react-native";
import { connect } from "react-redux";
import   {MovieRequested}  from "./redux/actions/movieActions"; 
import DetailItem from "./component/DetailItem" ;
interface VerificationCodeDigits {
  navigation: any; 
  movieState: any;
  Movie:any;
  year:String, 
  title:String, 
} 
function HomeScreen   (props: VerificationCodeDigits) {
    const [formData, setFormData] = useState({
      year: "", 
      search: "",
      idIMDB:""
    });
    useEffect(() => { 
      props.Movie(formData);

      }, []);


   async function callAPI (_type: number) {
      if(_type == 0)
      {
        if (formData.search == '')
        Alert.alert('please fill the title input')
      }else{
        if (formData.idIMDB == '')
        Alert.alert('please fill the idIMDB input')
      }


     props.Movie(formData);
     setFormData({
      year: "", 
      search: "",
      idIMDB:""
    })
      }

        const renderCategory = ({item} : {item:any}) => (
          <DetailItem 
            item={item} 
            />

         
        );

          return (
            <ScrollView contentContainerStyle={{alignItems:'center' , height:'100%' , justifyContent:'space-around'}}>
              <View style={{marginVertical:20 , justifyContent:'center' , alignItems:'center' , flexDirection:'row' , width:'100%' }}>

              <TextInput 
                style = {{width:120 , marginTop:2 , borderWidth: 1 , marginHorizontal:5 , borderRadius:5}}
                value={formData.year}
                placeholder = "Year"
                keyboardType = "numeric"
                underlineColorAndroid = "transparent" 
                onChangeText = {(value) => { setFormData({ ...formData, year: value }) }}
              />

              <TextInput 
                style = {{width:120 , marginTop:2 , borderWidth: 1 , marginHorizontal:5 , borderRadius:5}}
                value={formData.search}
                placeholder = "title"
                underlineColorAndroid = "transparent"
                onChangeText = {(value) => { setFormData({ ...formData, search: value.toLowerCase() }) }}
              />

              </View>

              <TouchableOpacity onPress={()=> callAPI(0)}
                  style={{width:'60%' , height:40 , backgroundColor:'yellow' , alignSelf:'center' , justifyContent:'center' , alignItems:'center' , borderRadius:20, marginVertical:20}}>
                  <Text>Search By name or year</Text>
              </TouchableOpacity>

              <TextInput 
                style = {{width:120 , marginTop:2 , borderWidth: 1 , marginHorizontal:5 , borderRadius:5}}
                value={formData.idIMDB}
                placeholder = "IMDB ID"
                underlineColorAndroid = "transparent"
                onChangeText = {(value) => { setFormData({ ...formData, idIMDB: value.toLowerCase() }) }}
              />
              
              <TouchableOpacity onPress={()=> callAPI(1)}
                  style={{width:'60%' ,  height:40 , backgroundColor:'yellow' , alignSelf:'center' , justifyContent:'center' , alignItems:'center' , borderRadius:20, marginVertical:20}}>
                  <Text>Search by IMDB ID</Text>
              </TouchableOpacity>

              <FlatList
              horizontal
              data= {props.movieState?.data?.Search}
              style={{height:250}}
              numColumns = {1}
              extraData ={props.movieState?.data?.Search}
              keyExtractor= {(item,index)=>index.toString()}
              renderItem= {renderCategory}
              />
              <FlatList
              horizontal
              data= {[props.movieState?.data]}
              numColumns = {1}
              extraData ={props.movieState?.data?.Search}
              keyExtractor= {(item,index)=>index.toString()}
              renderItem= {renderCategory}
              />
              <Text style={{textAlign:'center' , color:'red' }}>{JSON.stringify(props.movieState?.data?.Error)}</Text>                  

            </ScrollView>

          );
}
  function mapStateToProps(state: { movie: any; }) {
    return {
      movieState: state.movie,
    };
  }
  
  const mapDispatchToProps = (dispatch: (arg0: { payload: any; type: string; }) => any) => {
    return {
      Movie: (input: any) => dispatch(MovieRequested(input)),
    }
  }
  export default connect(mapStateToProps, mapDispatchToProps)(HomeScreen);


