
import React, { useState } from 'react';
import { Text, Image, TouchableOpacity } from "react-native";
import FastImage from 'react-native-fast-image';

const DetailItem = ({item}) => {

    return (
      <TouchableOpacity  style={{justifyContent:'center' , alignItems:'center' , width: 120, marginHorizontal:20 }}>
      <FastImage
          style={{ width: 120, height: 150 }}
          source={{
              uri: item?.Poster,
              priority: FastImage.priority.normal,
              }}
          resizeMode={FastImage.resizeMode.contain}
          />
      <Text >{item?.Title}</Text>                  
      <Text >{item?.Year}</Text>                  
  </TouchableOpacity>       
    );
  }
  export default React.memo(DetailItem)