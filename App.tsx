import React from 'react';
import {Provider} from 'react-redux';
import { store, persistor } from "./src/redux/store/store";
import { PersistGate } from 'redux-persist/integration/react'
import MainStackNavigator from './src/navigation/AppNavigator'

export type Props = {};

const App: React.FC<Props> = ({}) => {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
      <MainStackNavigator />
       
      </PersistGate>
    </Provider>
  );
}

export default App;


